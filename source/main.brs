' ********** Copyright 2020 MerakWeb.  All Rights Reserved. **********

sub RunUserInterface()
	screen = CreateObject("roSGScreen")
	scene = screen.CreateScene("HomeScene")
	port = CreateObject("roMessagePort")
	screen.SetMessagePort(port)
	screen.Show()

	list = [
		{
			Title: "Internacionales"
			ContentList: "Internacional"
		}
		{
			Title: "Nacionales"
			ContentList: "Nacional"
		}
		{
			Title: "Regionales"
			ContentList: "Regional"
		}
		{
			Title: "Radio"
			ContentList: "Radio"
		}
		{
			Title: "TV"
			ContentList: "TV"
		}
	]
	scene.gridContent = ParseXMLContent(list)

	while true
		msg = wait(0, port)
		print "------------------"
		print "msg = "; msg
	end while

	if screen <> invalid then
		screen.Close()
		screen = invalid
	end if
end sub

function ParseXMLContent(list as object)
	RowItems = createObject("RoSGNode", "ContentNode")
	Channels = canales()

	for each rowAA in list
		row = createObject("RoSGNode", "ContentNode")
		row.Title = rowAA.Title

		for each itemAA in Channels

			if itemAA.Categories = rowAA.ContentList
				item = createObject("RoSGNode", "ContentNode")
				' We don't use item.setFields(itemAA) as doesn't cast streamFormat to proper value
				for each key in itemAA
					item[key] = itemAA[key]
				end for
				row.appendChild(item)
			end if
		end for
		RowItems.appendChild(row)
	end for

	return RowItems
end function


function canales() as object
	searchRequest = CreateObject("roUrlTransfer")
	url = "http://tv.merakweb.co/wp-json/mrk/v1/canales"
	searchRequest.SetURL(url)
	response = ParseJson(searchRequest.GetToString())

	arr = []
	For Each item In response
		aux = {
			Title: item.Title
			Description: item.Description
			streamFormat: item.StreamFormat
			hdBackgroundImageUrl: item.Background 
			FHDPosterUrl: item.Poster
			url: item.URL 
			Categories: item.Category
		}

    	arr.push(aux)
	End For

	return arr
end function
