' ********** Copyright 2020 MerakWeb.  All Rights Reserved. **********
' inicializar la grilla
' crear los hijos
' activar los observadores
function Init()
	' escuchar en el puerto 8089
	? "[HomeScene] Init"

	' Nodo de la grill
	m.GridScreen = m.top.findNode("GridScreen")

	' Nodo del reproductor de video
	m.videoPlayer = m.top.findNode("videoPlayer")

	' Posición del poster, sistema de animación del popster y detección de inicio de una iteración
	' de un ciclo
	m.Background = m.top.findNode("Background")
	m.PosterPosition = m.top.findNode("PosterPosition")
	m.PosterPosition.observeField("fire", "posterPosition")

	' Stream timeout
	m.StreamTimeout = m.top.findNode("StreamTimeout")
	m.StreamTimeout.observeField("fire", "streamTimeout")

	' URL del stream que estamos observando. Se usa para no tener que
	' cargar d enuevo un stream si es el que actualmente estamos viendo
	m.currentStream = ""
	m.isRadio = false

	' Manejador para el evento de selección de item en la grilla
	m.top.observeField("rowItemSelected", "OnRowItemSelected")
	m.top.observeField("itemFocused", "OnItemFocused")

	' Manejador para el evento de visualización de carga de datos
	m.loadingIndicator = m.top.findNode("loadingIndicator")

	' Inicializar datos analytics
	SetupAnalytics()
end function

sub SetupAnalytics()
	m.global.addField("RSG_analytics", "node", false)
	m.global.RSG_analytics = CreateObject("roSGNode", "Roku_Analytics:AnalyticsNode")
	'disables all debug prints, enable it in order to see which events are fired
	' m.global.RSG_analytics.debug = true

	' Analytics Initialization
	m.global.RSG_analytics.init = {

		'set data to IQ analytics
		Google: {
			trackingID: "UA-178439612-1"
			defaultParams: {
				an: "MerakWebTV"
			}
		}
	}

	' Informar que hay acceso
	m.global.RSG_analytics.trackEvent = {
		Google: {
			t: "event"
			ec: "App"
			ea: "Open"
			el: createObject("roDeviceInfo").GetChannelClientId()
		}
	}

end sub

' Función para determinar si un texto está entre los items de un arreglo
function Contains(arr as object, value as string) as boolean
	for each entry in arr
		if entry = value
			return true
		end if
	end for
	return false
end function

' Cambio de la posición del poster
sub posterPosition()
	' Buscar una poisición aleatoria que no deje el poster por fuera
	m.Background.translation = "[" + Str(Rnd(1920 - 267 - 500) + 250).Trim() + "," + Str(Rnd(1080 - 150 - 300) + 150).Trim() + "]"
end sub

' Revisar si el canal corrió bien o ya pasó el tiempo
sub streamTimeout()
	' Determinar si sigue en estado bufering después del tiempo máximo
	if m.videoPlayer.state = "buffering" or m.videoPlayer.state = "error" or m.videoPlayer.state = "none"
		' Detener el video
		m.videoPlayer.control = "stop"
		m.videoPlayer.visible = "false"

		' Mostrar la grilla
		m.GridScreen.visible = true
		m.GridScreen.setFocus(true)

		' Detener poster de radio
		m.PosterPosition.control = "stop"
		m.Background.visible = false

		' Olvidar el actual servicio de streaming
		m.currentStream = ""

		' Si hubo error, indicar el nombre del canal
		m.global.RSG_analytics.trackEvent = {
			Google: {
				t: "event"
				ec: m.videoPlayer.content.title
				ea: "error"
			}
		}

		' Mostrar el mensaje de error
		showError()
	else
		' Si se está reproduciendo, indicar el nombre del canal
		m.global.RSG_analytics.trackEvent = {
			Google: {
				t: "event"
				ec: m.videoPlayer.content.title
				ea: "ok"
			}
		}
	end if
end sub

' Manejo de selección de un item de la grilla
sub OnRowItemSelected()
	? "[HomeScene] OnRowItemSelected"

	' Detener el mecanismo de stream timeout por si había uno
	m.StreamTimeout.control = "stop"

	' Ocultar la grilla
	m.GridScreen.visible = "false"

	'Determinar cuál contenido se visualizará
	selectedItem = m.GridScreen.focusedContent

	' Detener la animación del poster y ocultarlo
	' Tal vez estábamos escuchando una radio así que supondremos este caso
	m.PosterPosition.control = "stop"
	m.Background.visible = false

	' Ubicar el poster en el centro de la pantalla
	m.Background.translation = "[" + Str((1920 - 267) / 2).Trim() + "," + Str((1080 - 150) / 2).Trim() + "]"

	' Usar la imagen del poster del conenido a ser visualizado
	m.Background.uri = selectedItem.FHDPosterUrl

	' Retornar reproductor a tamaño completo
	m.videoPlayer.translation = "[0,0]"
	m.videoPlayer.width = "1920"
	m.videoPlayer.height = "1080"

	' ¿Es el contenido a visualizar de la categoría 'Radio'?
	if Contains(selectedItem.Categories, "Radio")
		' Visualizar el poster e inicializar la animación
		m.Background.visible = true
		m.PosterPosition.control = "start"
		m.isRadio = true
	else
		m.isRadio = false
	end if

	' Si la url es distinta a la que está almacenada entonces será necesario
	' inicializar el reproductor de video con la nueva información
	if m.currentStream <> selectedItem.url
		' Hacer visible el reproductor porque tal vez estémos en el estado inicial
		' que no tiene visible el reproductor
		m.videoPlayer.visible = true

		' Asignar el contenido del item seleccionado
		m.videoPlayer.content = selectedItem

		' Ubicar el reproductor a 2 horas en el stream. Tal vez es un stream de esos que
		' tienen hasta 2 horas de propgrama previo, pero queremos ir directamente al estado
		'en vivo
		m.videoPlayer.seek = 60 * 60 * 2

		' Reproducir el video
		m.videoPlayer.control = "play"

		' Guardamos la url para comparar en el futuro si es el canal que se está reproduciendo en
		' el minimizado
		m.currentStream = selectedItem.url
	end if

	' Iniciar el manejo del timeout del canal
	m.StreamTimeout.control = "start"

	' Indicar que se seleccionó el canal
	m.global.RSG_analytics.trackEvent = {
		Google: {
			t: "event"
			ec: m.videoPlayer.content.title
			ea: "select"
		}
	}

	'Darle foco al reproductor de video
	m.videoPlayer.setFocus(true)
	m.videoPlayer.observeField("state", "OnVideoPlayerStateChange")

end sub

' Atender evento de cambio de estado del reproducto de video
sub OnVideoPlayerStateChange()
	? "HomeScene > OnVideoPlayerStateChange : state == ";m.videoPlayer.state
	if m.videoPlayer.state = "error" ' Hubo un error
		' Detener poster de radio
		m.PosterPosition.control = "stop"
		m.Background.visible = false

		' Ocultar el reproductor de video
		m.videoPlayer.visible = false

		' Asignar el foco a la grilla
		m.GridScreen.visible = true
		m.GridScreen.setFocus(true)

		' Olvidar el actual servicio de streaming
		m.currentStream = ""

		' Mostrar el diálogo del error
		showError()

	else if m.videoPlayer.state = "playing" 'Se está reproduciendo el stream
	else if m.videoPlayer.state = "finished" ' La reproducción terminó
		' Ocultar el reproductor de video
		m.videoPlayer.visible = false

		'Asignar el foco a la grilla
		m.GridScreen.visible = true
		m.GridScreen.setFocus(true)
	end if
end sub

' Al cambiar el contenido, asignar foco a la grilla
sub OnChangeContent()
	? "OnChangeContent"

	' Dar foco a la grila
	m.GridScreen.setFocus(true)

	' Detener el indicador
	m.loadingIndicator.control = "stop"
end sub

sub showAbout()
	dialog = createObject("roSGNode", "Dialog")
	dialog.title = "MerakWebTV"
	dialog.optionsDialog = true
	dialog.message = "Está aplicación fue desarrollada para visualizar canales y emisoras públicas y regionales de distribución gratuita en Colombia y para servicios de noticias internacionales y canales de streaming libres." + chr(10) + chr(10) + "Cualquier solicitud que incumpla estos requisitos será descartada." + chr(10) + chr(10) + "Contacte a tv@merakweb.co para informar un error o para sugerir un nuevo canal."
	dialog.buttons = [
		"OK"
	]
	m.top.dialog = dialog

	m.top.dialog.observeField("buttonSelected", "OnButtonDialog")
end sub

sub showError()
	dialog = createObject("roSGNode", "Dialog")
	dialog.title = "Lo sentimos"
	dialog.optionsDialog = true
	dialog.message = "Esta señal no está transmitiendo en este momento. Inténtelo más tarde."
	dialog.buttons = [
		"Seleccionar otro canal",
		"Reportar esta señal"
	]
	m.top.dialog = dialog

	m.top.dialog.observeField("buttonSelected", "OnButtonDialog")
end sub

sub OnButtonDialog()
	if m.top.dialog.buttonSelected = 0
		m.top.dialog.close = true
	else if m.top.dialog.buttonSelected = 1
		m.top.dialog.close = true
		showAbout()
	end if
end sub

' Main Remote keypress event loop
function OnkeyEvent(key, press) as boolean
	? ">>> HomeScene >> OnkeyEvent"
	' Por defecto saldremos del programa si no entendemos la tecla que se presionó
	result = false

	if press
		? "key == ";key

		' Atender eventos de opciones
		if key = "options"
			showAbout()
			' ¿Es el botón play mientras se ve el reproductor y la grilla?
		else if m.GridScreen.visible = true and m.videoPlayer.visible = true and key = "play"
			' Ocultar la grilla
			m.GridScreen.visible = "false"

			' Ampliar video a toda la pantalla y asignarle el foco
			m.videoPlayer.translation = "[0,0]"
			m.videoPlayer.width = "1920"
			m.videoPlayer.height = "1080"
			m.videoPlayer.setFocus(true)

			' Ocultar poster
			m.Background.visible = false
			' Si es radio, inicializar poster en el centro de la pantalla, visiaulizarlo e iniciar la animación
			' del poster por la pantalla
			selectedItem = m.GridScreen.focusedContent
			if m.isRadio
				m.Background.translation = "[" + Str((1920 - 267) / 2).Trim() + "," + Str((1080 - 150) / 2).Trim() + "]"
				m.Background.visible = true
				m.PosterPosition.control = "start"
			end if

			' No salir del programa
			result = true

			' ¿Es el botón back mientras se ve el reproductor y la grilla?
		else if m.GridScreen.visible = true and m.videoPlayer.visible = true and key = "back"
			' Detener el manejo del timeout del canal
			m.StreamTimeout.control = "stop"

			' Ocultar reproductor de video y detenerlo
			m.videoPlayer.visible = false
			m.videoPlayer.control = "stop"

			' Reiniciamos en estado vacio la variable donde almacenamos el
			' stream que se está reproduciendo porque ya no estamos viendo video
			m.currentStream = ""

			' Ocultar el poster
			m.Background.visible = false

			' Asignar foco a la grilla
			m.GridScreen.setFocus(true)

			' No salir del programa
			result = true

			' ¿Es el botón back mientras se ve el video a pantalla completa?
		else if m.GridScreen.visible = false and key = "back"
			' Es posible que sea una emisora de radio, así que detendremos la animación del poster
			' y la ubicaremops en la posición centrada en el recuadro de video minimizado
			' Si estaba visible (es decir esta es una emisora de radio) entonces se verá
			' ahora en la posición adecuada
			m.PosterPosition.control = "stop"
			m.Background.translation = "[1355, 238]"

			' Minimizar la pantalla de video y ubicarla en la posición minimizada
			m.videoPlayer.translation = "[1200, 150]"
			m.videoPlayer.width = "573"
			m.videoPlayer.height = "324"

			' Hacer visible la grilla de canales y asignarle el foco
			m.GridScreen.visible = true
			m.GridScreen.setFocus(true)

			' No salir del programa
			result = true
		end if
	end if

	return result
end function
